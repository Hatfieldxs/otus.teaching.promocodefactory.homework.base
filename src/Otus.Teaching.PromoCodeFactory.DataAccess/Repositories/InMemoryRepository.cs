﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task AddAsync(T item)
        {
            Action adding = () => ((ICollection<T>)Data).Add(item);

            await Task.Run(() => DoingConcurency(adding));          
        }

        public async Task UpdateAsync(T item)
        {          
            Action updating = () =>
            {
                T foundedItem = Data.FirstOrDefault(x => x.Id == item.Id);
                if (foundedItem != null)
                {
                    ((ICollection<T>)Data).Remove(foundedItem);

                    ((ICollection<T>)Data).Add(foundedItem);
                }
            };

            await Task.Run(() => DoingConcurency(updating));              
        }

        public async Task DeleteAsync(Guid id)
        {         
            Action removing = () =>
            {
                T foundedItem = Data.FirstOrDefault(x => x.Id == id);
                if (foundedItem != null)
                {
                    ((ICollection<T>)Data).Remove(foundedItem);
                }
                            
            };            
            
            await Task.Run(() => DoingConcurency(removing));
            
        }

        private void DoingConcurency(Action action)
        {  
            lock (Data)
            {
                action?.Invoke();
            }
        }
    }
}