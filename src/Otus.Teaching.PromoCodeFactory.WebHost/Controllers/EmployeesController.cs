﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Create new employee
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] EmployeeResponse model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("model data isnt valid");
            }

            var nameMap = model.FullName.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            //TODO implement validation
            if (nameMap.Count() != 2)
            {
                return BadRequest("full name must have template: {first_name} {last_name}");
            }

            //TODO implement automapper
            //TODO изменить логику с ролями. Роли должны создаваться через RolesController. Здесь должен быть только поиск по GUID
            await _employeeRepository.AddAsync(
                new Employee
                {
                    Id = Guid.NewGuid(),
                    FirstName = nameMap[0],
                    LastName = nameMap[1],
                    AppliedPromocodesCount = model.AppliedPromocodesCount,
                    Roles = model.Roles
                    .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                        .Select(x => new Role 
                                {   Id = new Guid(), 
                                    Name = x.Name, 
                                    Description = x.Description 
                                }).ToList()

                });

            return Ok("success");
        }

        /// <summary>
        /// Update existed employee
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] EmployeeResponse model)
        {
            var nameMap = model.FullName.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            //TODO implement validation
            if (nameMap.Count() != 2)
            {
                return BadRequest("full name must have template: {first_name} {last_name}");
            }

            //TODO implement automapper
            //TODO изменить логику с ролями. Роли должны создаваться через RolesController. Здесь должен быть только поиск по GUID
            await _employeeRepository.UpdateAsync(
                new Employee
                {
                    Id = model.Id,
                    FirstName = nameMap[0],
                    LastName = nameMap[1],
                    AppliedPromocodesCount = model.AppliedPromocodesCount,
                    Roles = model.Roles
                    .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                        .Select(x => new Role
                        {
                            Id = Guid.NewGuid(),
                            Name = x.Name,
                            Description = x.Description
                        }).ToList()

                });

            return Ok("success");
        }

        /// <summary>
        /// Delete existed employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);

            return Ok("success");
        }

    }
}